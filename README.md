# MyMonitoring

## explication

Une application web pour centraliser les informations d'un projet qui concernent tout le monde, mais surtout moi.
Dans un projet, je ne suis pas à tous les postes et n'ai pas toutes les responsabilités.
C'est pourquoi il faut savoir se remettre à sa place.
Dans un but d'autoformation et prise de conscience, MyMonitoring va guider la façon de pensée "Projet".
Afin de mieux aborder tous les points de vue disponibles et de mieux comprendre ce que les autres font sans avoir à le faire à leur place.

On récupère toutes nos informations, puis on les classe, et enfin on réapprend à lire pour mieux comprendre. À chaque point de vue, on va retirer ce qui est superflu.
En tant que dev, lorsque je suis à la place du finassier, je ne vois que les sous, c'est à dire pas grand-chose.
Je sais donc ce que je dois faire.
Car un copilote doit pouvoir intervenir sur tous les points d'un projet, pas seulement ceux qui le concernent directement.

Dans un but d'apprentissage de la gestion de projet mais aussi de développement C#, j'ai initié ce projet.

Pour ce faire je respect plusieurs normes :

## I - Principes de développement

### A - KISS (Keep It Simple, Stupid)

### B - DRY (Don't Repeat Yourself)

### C - SOLID

Responsabilité unique (Single responsibility principle)

- Une classe, une fonction ou une méthode doit avoir une et une seule responsabilité.
- Posez-vous toujours la question (toujours, vraiment : toujours [vraiment]).

Ouvert/fermé (Open/closed principle)

- Une entité applicative (class, fonction, module ...) doit être ouverte à l'extension, mais fermée à la modification.

Substitution de Liskov (Liskov substitution principle)

- Une instance de type T doit pouvoir être remplacée par une instance de type G, tel que G sous-type de T, sans que cela ne modifie la cohérence du programme

Ségrégation des interfaces (Interface segregation principle)

- Préférer plusieurs interfaces spécifiques pour chaque client plutôt qu'une seule interface générale

  > The interface-segregation principle (ISP) states that no client should be forced to depend on methods it does not use. (Robert Martin)

- This principle comes naturally when you start decomposing your problem space with identifying major roles that take part in your domain. And it’s never a mechanical action. No principle can automatically lead you to the correct object model.

Inversion des dépendances (Dependency inversion principle)

- Il faut dépendre des abstractions, pas des implémentations.
- Attention : ce n'est pas juste "on injecte nos services".

## II - Architecture applicative

### RESTful

- Representational State Transfer (REST) is usually defined as a software architecture style that applies some constraints to a web service.
- The REST approach to web service architecture became increasingly popular because it is straightforward and clear. Unlike some old methods, such as the Simple Object Access Protocol (SOAP) or the Windows Communication Foundation (WCF), REST services provide a clean way of querying data. Different information can be obtained using different URIs without the need for adding any overhead to requests.

#### REST requirements (5 contraintes)

##### 1 - Client-serveur

Les serveurs exposent des ressources que les clients souhaitent utiliser.
Les serveurs gèrent le stockage et la "business-logic".
Les clients gèrent les UI.

##### 2 - Stateless

Les informations concernant la session du client sont stockées chez lui.
En conséquence, chaque requête doit contenir l'intégralité des informations nécessaires à son traitement (auth. etc.).

##### 3 - Cache

Le serveur doit présenter des ressource dont la mise en cache est possible / pas possible.
Réduit considérablement les besoin en ressources et les interactions client serveurs.

##### 4 - / ! \ Uniform interface / ! \

Clé d'une architecture REST, découle des principes précédents.
C'est ce principe qui permet une compréhension globale quelque soit le service utilisé (banque, maps, information etc.).

Est composée de 4 sous contraintes :

- 4.1. Identification of Resources

  - Centré autour de la notion de ressource (les entités du domaine [pas de la base de données]).

  - Chaque ressource dans une conception RESTful doit être identifiable de manière unique via une URI.

    - <https://api.example.com/customers>
    - <https://api.example.com/customers/932612>

- 4.2 Manipulation of Resources through Representations

- Le client n'interagis jamais avec les ressources en direct.

  > {
  >
  > **"id":** 12,
  >
  > **"firstname":** "Han",
  >
  > **"lastname":**"Solo"
  >
  > }

- Bénéfice : aucun couplage entre le client et le serveur.

4.3 Self-Descriptive Messages

- Chaque message (demande/réponse) doit comporter suffisamment d'informations pour que le destinataire puisse le comprendre isolément.
- Headers / Verbs / Path / Body (permis par le protocole HTTP, mais pas obligatoire).

4.4 Hypermedia as the Engine of Application State

> {
>
> **"id":** 12,
>
> **"firstname":** "Han",
>
> **"lastname":** "Solo",
>
> **"_links":** {
>
> **"self":** {
>
> **"href":** "https://api.example.com/customers/12"
>
> },
>
> **"orders":** {
>
> **"href":** "https://api.example.com/customers/12/orders"
>
> }
>
> }
>
> }

## III - Objectifs de l'application

Construire une structure applicative respectant l'ensemble des principes précités.

**Utiliser, pour ça, les outils fournis par ASP.NET Core API :**

- Middleware Pipeline
- Dependency Injection System
- Routing System
- Filter Pipeline

**Respecter une architecture applicative à l'état de l'art :**

- Onion architecture.

- Orientée domaine.

- Séparation claire entre les couches DOMAINE et INFRASTRUCTURE.

**Trois étapes (features):**

1 - Data Access Layer

- Designing project entities
- Implementing a data access layer using EF Core
- Testing a data layer using the in-memory database

2 - Domain logic

- Implement the service classes for our application
- Implement request DTOs and the related validation system
- Apply tests to verify the implemented logic

3 - RESTful HTTP Layer

- Implementing the HTTP layer of a service
- Carrying out tests using the tools provided by ASP.NET Core
- Improving the resilience of the HTTP layer.
