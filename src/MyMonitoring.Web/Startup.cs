using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using MyMonitoring.Domain.Extensions;
using MyMonitoring.Web.Services;
using MyMonitoring.Web.Extensions;
using Microsoft.AspNetCore.Http;
using MyMonitoring.Infrastructure.Extensions;
using MyMonitoring.Domain.Entities.Admin;

namespace MyMonitoring.Web
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMonitoringContext(Configuration.GetConnectionString("DefaultConnection"));
            
            services.AddControllersWithViews();
            services.AddRazorPages();

            services.AddScoped<SignInManager<ApplicationUser>, SignInManager<ApplicationUser>>();
            
            //services.AddTransient<IEmailSender, EmailSender>();
            //services.Configure<AuthMessageSenderOptions>(Configuration);

            services.ConfigureApplicationCookie(o => {
                o.ExpireTimeSpan = TimeSpan.FromDays(5);
                o.SlidingExpiration = true;
            });

            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential 
                // cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });

            services.Configure<DataProtectionTokenProviderOptions>(o =>
                o.TokenLifespan = TimeSpan.FromHours(3));

            services
                .AddRepositories()
                .AddMappers()
                .AddServices()
                .AddControllers()
                .AddValidation();

            services.AddServerSideBlazor();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseCookiePolicy();

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "lot",
                    pattern: "Project/{projectId}/Lot/{action=Index}/{id?}",
                    defaults: new { controller = "Lot" });
                endpoints.MapControllerRoute(
                    name: "feature",
                    pattern: "Lot/{lotId}/{action=Index}/{id?}",
                    defaults: new { controller = "Feature" });
                endpoints.MapControllerRoute(
                    name: "issue",
                    pattern: "Feature/{featureId}/{action=Index}/{id?}",
                    defaults: new { controller = "Issue" });
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");

                endpoints.MapRazorPages();
                endpoints.MapBlazorHub();
            });
        }
    }
}
