﻿using System;
using MyMonitoring.Domain.Entities.Admin;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MyMonitoring.Domain.Responses.Project;
using MyMonitoring.Domain.Services;
using MyMonitoring.Domain.Services.Interfaces;
using MyMonitoring.Domain.Requests.Project;
using Microsoft.AspNetCore.Identity;

namespace MyMonitoring.Web.Controllers
{
    [Authorize]
    public class ProjectController : Controller
    {
        private readonly IProjectService _projectService;
        private readonly UserManager<ApplicationUser> _userManager;

        public ProjectController(IProjectService projectService, UserManager<ApplicationUser> userManager)
        {
            _projectService = projectService;
            _userManager = userManager;
        }

        // GET: Project
        [AllowAnonymous]
        public async Task<ActionResult> Index()
        {
            return View(await _projectService.GetProjectsAsync());
        }

        // GET: Project/Details/5
        public async Task<ActionResult> Details(Guid id)
        {
            return View(await _projectService.GetProjectAsync(
                new GetProjectRequest()
                {
                    Id = id
                }));
        }

        // GET: Project/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Project/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(IFormCollection collection)
        {
            try
            {
                AddProjectRequest request = new AddProjectRequest()
                {
                    Name = collection["Name"].ToString(),
                    Comment = collection["Comment"].ToString(),
                    StartDate = DateTime.Parse(collection["StartDate"]),
                    EndDate = DateTime.Parse(collection["EndDate"]),
                    OwnerId = Guid.Parse(_userManager.GetUserId(User))
                };

                await _projectService.AddProjectAsync(request);

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                ProjectResponse response = new ProjectResponse()
                {
                    Name = collection["Name"].ToString(),
                    Comment = collection["Comment"].ToString(),
                    StartDate = DateTime.Parse(collection["StartDate"]),
                    EndDate = DateTime.Parse(collection["EndDate"])
                };
                return View(response);
            }
        }

        // GET: Project/Edit/5
        public async Task<ActionResult> Edit(Guid id)
        {
            return View(await _projectService.GetProjectAsync(
                new GetProjectRequest()
                {
                    Id = id
                }));
        }

        // POST: Project/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Guid id, IFormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: Project/Delete/5
        public ActionResult Delete(Guid id)
        {
            return View();
        }

        // POST: Project/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(Guid id, IFormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}