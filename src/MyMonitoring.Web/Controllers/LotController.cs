﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using MyMonitoring.Domain.Entities.Admin;
using MyMonitoring.Domain.Requests.Lot;
using MyMonitoring.Domain.Requests.Project;
using MyMonitoring.Domain.Responses.Lot;
using MyMonitoring.Domain.Responses.Project;
using MyMonitoring.Domain.Services.Interfaces;

namespace MyMonitoring.Web.Controllers
{
    public class LotController : Controller
    {
        private readonly ILotService _lotService;
        private readonly IProjectService _projectService;
        private readonly UserManager<ApplicationUser> _userManager;

        public LotController(ILotService lotService, IProjectService projectService, UserManager<ApplicationUser> userManager)
        {
            _lotService = lotService;
            _projectService = projectService;
            _userManager = userManager;
        }

        // GET: LotController
        public async Task<ActionResult> Index(Guid projectId)
        {
            ViewBag.Project = await _projectService.GetProjectAsync(new GetProjectRequest() { Id = projectId });
            return View(await _lotService.GetLotsAsync());
        }

        // GET: LotController/Details/5
        public async Task<ActionResult> Details(Guid id)
        {
            return View(await _lotService.GetLotAsync(
                new GetLotRequest()
                {
                    Id = id
                }));
        }

        // GET: LotController/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: LotController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(Guid projectId, IFormCollection collection)
        {
            try
            {
                AddLotRequest request = new AddLotRequest()
                {
                    Name = collection["Name"].ToString(),
                    Description = collection["Description"].ToString(),
                    ProjectId = projectId
                };

                await _lotService.AddLotAsync(request);

                return RedirectToAction(nameof(Index));
            }
            catch(Exception ex)
            {
                ViewBag.Error = ex.Message;

                LotResponse response = new LotResponse()
                {
                    Name = collection["Name"].ToString(),
                    Description = collection["Description"].ToString(),
                };
                return View(response);
            }
        }

        // GET: LotController/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: LotController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: LotController/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: LotController/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}
