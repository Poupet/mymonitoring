﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using MyMonitoring.Domain.Entities.Admin;
using MyMonitoring.Domain.Requests.Feature;
using MyMonitoring.Domain.Requests.Issue;
using MyMonitoring.Domain.Responses.Issue;
using MyMonitoring.Domain.Services.Interfaces;

namespace MyMonitoring.Web.Controllers
{
    public class IssueController : Controller
    {
        private readonly IIssueService _issueService;
        private readonly IFeatureService _featureService;
        private readonly UserManager<ApplicationUser> _userManager;

        public IssueController(IIssueService issueService, IFeatureService featureService, UserManager<ApplicationUser> userManager)
        {
            _issueService = issueService;
            _featureService = featureService;
            _userManager = userManager;
        }

        // GET: IssueController
        public async Task<ActionResult> Index(Guid featureId)
        {
            ViewBag.Feature = await _featureService.GetFeatureAsync(new GetFeatureRequest(){ Id = featureId });
            return View(await _issueService.GetIssuesAsync());
        }

        // GET: IssueController/Details/5
        public async Task<ActionResult> Details(Guid id)
        {
            return View(await _issueService.GetIssueAsync(
                new GetIssueRequest()
                {
                    Id = id
                }));
        }

        // GET: IssueController/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: IssueController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(Guid featureId, IFormCollection collection)
        {
            System.Diagnostics.Debug.WriteLine(collection["Finished"]);
            System.Diagnostics.Debug.WriteLine(collection["Finished"]);
            System.Diagnostics.Debug.WriteLine(collection["Finished"]);
            System.Diagnostics.Debug.WriteLine(collection["Finished"]);
            try
            {
                AddIssueRequest request = new AddIssueRequest()
                {
                    Name = collection["Name"].ToString(),
                    Description = collection["Description"].ToString(),
                    FeatureId = featureId,
                    Finished = Convert.ToBoolean(collection["Finished"].ToString().Split(',')[0]),
                    FinisherId = Guid.Parse(_userManager.GetUserId(User))
                };

                await _issueService.AddIssueAsync(request);

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                IssueResponse response = new IssueResponse()
                {
                    Name = collection["Name"].ToString(),
                    Description = collection["Description"].ToString(),
                    Finished = Convert.ToBoolean(collection["Finished"].ToString().Split(',')[0]),
                };
                return View(response);
            }
        }

        // GET: IssueController/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: IssueController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: IssueController/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: IssueController/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}
