﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using MyMonitoring.Domain.Entities.Admin;
using MyMonitoring.Domain.Requests.Feature;
using MyMonitoring.Domain.Requests.Lot;
using MyMonitoring.Domain.Responses.Feature;
using MyMonitoring.Domain.Services.Interfaces;

namespace MyMonitoring.Web.Controllers
{
    public class FeatureController : Controller
    {
        private readonly IFeatureService _featureService;
        private readonly ILotService _lotService;
        private readonly UserManager<ApplicationUser> _userManager;

        public FeatureController(IFeatureService featureService, ILotService lotService, UserManager<ApplicationUser> userManager)
        {
            _featureService = featureService;
            _lotService = lotService;
            _userManager = userManager;
        }

        // GET: FeatureController
        public async Task<ActionResult> Index(Guid lotId)
        {
            ViewBag.Lot = await _lotService.GetLotAsync(new GetLotRequest(){ Id = lotId });
            return View(await _featureService.GetFeaturesAsync());
        }

        // GET: FeatureController/Details/5
        public async Task<ActionResult> Details(Guid id)
        {
            return View(await _featureService.GetFeatureAsync(
                new GetFeatureRequest()
                {
                    Id = id
                }));
        }

        // GET: FeatureController/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: FeatureController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(Guid lotId, IFormCollection collection)
        {
            try
            {
                AddFeatureRequest request = new AddFeatureRequest()
                {
                    Name = collection["Name"].ToString(),
                    Description = collection["Description"].ToString(),
                    LotId = lotId
                };

                await _featureService.AddFeatureAsync(request);

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                FeatureResponse response = new FeatureResponse()
                {
                    Name = collection["Name"].ToString(),
                    Description = collection["Description"].ToString(),
                };
                return View(response);
            }
        }

        // GET: FeatureController/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: FeatureController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: FeatureController/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: FeatureController/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}
