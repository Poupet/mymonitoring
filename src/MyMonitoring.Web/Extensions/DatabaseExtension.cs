﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using MyMonitoring.Domain.Entities.Admin;
using MyMonitoring.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyMonitoring.Web.Extensions
{
    public static class DatabaseExtension
    {
        public static IServiceCollection AddMonitoringContext(this IServiceCollection services, string connectionString)
        {
            services.AddDbContext<MonitoringContext>(options =>
                options.UseSqlServer(
                    connectionString,
                    serverOptions =>
                        {
                            serverOptions.MigrationsAssembly
                            (typeof(Startup).Assembly.FullName);
                        }
                    ));

            services.AddDefaultIdentity<ApplicationUser>(
                options => options.SignIn.RequireConfirmedAccount = true
                )
                .AddEntityFrameworkStores<MonitoringContext>()
                .AddDefaultTokenProviders()
                .AddDefaultUI();

            return services;
        }

    }
}
