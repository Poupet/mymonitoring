﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MyMonitoring.Web.Migrations
{
    public partial class Issue : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameTable(
                name: "Features",
                newName: "Features",
                newSchema: "monitoring");

            migrationBuilder.CreateTable(
                name: "Issues",
                schema: "monitoring",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    FeatureId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Issues", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Issues_Features_FeatureId",
                        column: x => x.FeatureId,
                        principalSchema: "monitoring",
                        principalTable: "Features",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Issues_FeatureId",
                schema: "monitoring",
                table: "Issues",
                column: "FeatureId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Issues",
                schema: "monitoring");

            migrationBuilder.RenameTable(
                name: "Features",
                schema: "monitoring",
                newName: "Features");
        }
    }
}
