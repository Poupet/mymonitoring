﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MyMonitoring.Web.Migrations
{
    public partial class Add_Data_Issue : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "visibility",
                schema: "monitoring",
                table: "Projects",
                newName: "Visibility");

            migrationBuilder.AddColumn<DateTime>(
                name: "EndDate",
                schema: "monitoring",
                table: "Issues",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<bool>(
                name: "Finished",
                schema: "monitoring",
                table: "Issues",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<Guid>(
                name: "FinisherId",
                schema: "monitoring",
                table: "Issues",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.CreateIndex(
                name: "IX_Issues_FinisherId",
                schema: "monitoring",
                table: "Issues",
                column: "FinisherId");

            migrationBuilder.AddForeignKey(
                name: "FK_Issues_AspNetUsers_FinisherId",
                schema: "monitoring",
                table: "Issues",
                column: "FinisherId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Issues_AspNetUsers_FinisherId",
                schema: "monitoring",
                table: "Issues");

            migrationBuilder.DropIndex(
                name: "IX_Issues_FinisherId",
                schema: "monitoring",
                table: "Issues");

            migrationBuilder.DropColumn(
                name: "EndDate",
                schema: "monitoring",
                table: "Issues");

            migrationBuilder.DropColumn(
                name: "Finished",
                schema: "monitoring",
                table: "Issues");

            migrationBuilder.DropColumn(
                name: "FinisherId",
                schema: "monitoring",
                table: "Issues");

            migrationBuilder.RenameColumn(
                name: "Visibility",
                schema: "monitoring",
                table: "Projects",
                newName: "visibility");
        }
    }
}
