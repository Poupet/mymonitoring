﻿using MyMonitoring.Domain.Repositories;
using MyMonitoring.Infrastructure.Repositories;
using Microsoft.Extensions.DependencyInjection;

namespace MyMonitoring.Infrastructure.Extensions
{
    public static class DependenciesRegistration
    {
        public static IServiceCollection AddRepositories(this IServiceCollection services)
        {
            services
                .AddScoped<IProjectRepository, ProjectRepository>()
                .AddScoped<ILotRepository, LotRepository>()
                .AddScoped<IFeatureRepository, FeatureRepository>()
                .AddScoped<IIssueRepository, IssueRepository>();

            return services;
        }
    }
}