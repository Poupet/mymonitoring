﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MyMonitoring.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace MyMonitoring.Infrastructure.SchemaDefinitions
{
    public class IssueEntityShemaDefinition : IEntityTypeConfiguration<Issue>
    {
        public void Configure(EntityTypeBuilder<Issue> builder)
        {
            builder.ToTable("Issues", MonitoringContext.DEFAULT_SCHEMA);
            builder.HasKey(k => k.Id);
            builder.Property(i => i.Name);
            builder.Property(i => i.Description);
            builder.Property(i => i.Finished);
            builder.Property(i => i.EndDate);

            builder.HasOne(i => i.Feature)
                .WithMany(f => f.Issues)
                .HasForeignKey(k => k.FeatureId);

            builder.HasOne(i => i.Finisher)
                .WithMany(u => u.Issues)
                .HasForeignKey(k => k.FinisherId)
                .OnDelete(DeleteBehavior.Restrict);
        }
    }
}
