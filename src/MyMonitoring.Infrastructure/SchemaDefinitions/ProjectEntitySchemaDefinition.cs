using MyMonitoring.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;

namespace MyMonitoring.Infrastructure.SchemaDefinitions
{
    public class ProjectEntitySchemaDefinition : IEntityTypeConfiguration<Project>
    {
        public void Configure(EntityTypeBuilder<Project> builder)
        {
            builder.ToTable("Projects", MonitoringContext.DEFAULT_SCHEMA);
            builder.HasKey(k => k.Id);
            builder.Property(p => p.Name).IsRequired().HasMaxLength(50);
            builder.Property(p => p.Comment).HasMaxLength(255);
            builder.Property(p => p.StartDate);
            builder.Property(p => p.EndDate);
            builder.Property(p => p.Visibility);
            builder.HasOne(p => p.Owner)
                .WithMany(p => p.Projects)
                .HasForeignKey(k => k.OwnerId);
        }
    }
}
