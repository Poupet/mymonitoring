﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MyMonitoring.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace MyMonitoring.Infrastructure.SchemaDefinitions
{
    public class FeatureEntityShemaDefinition : IEntityTypeConfiguration<Feature>
    {
        public void Configure(EntityTypeBuilder<Feature> builder)
        {
            builder.ToTable("Features", MonitoringContext.DEFAULT_SCHEMA);
            builder.HasKey(k => k.Id);
            builder.Property(l => l.Name);
            builder.Property(l => l.Description);

            builder.HasOne(l => l.Lot)
                .WithMany(p => p.Features)
                .HasForeignKey(k => k.LotId);
        }
    }
}
