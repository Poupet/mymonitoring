﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MyMonitoring.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace MyMonitoring.Infrastructure.SchemaDefinitions
{
    public class LotEntitySchemaDefinition : IEntityTypeConfiguration<Lot>
    {
        public void Configure(EntityTypeBuilder<Lot> builder)
        {
            builder.ToTable("Lots", MonitoringContext.DEFAULT_SCHEMA);
            builder.HasKey(k => k.Id);
            builder.Property(l => l.Name);
            builder.Property(l => l.Description);

            builder.HasOne(l => l.Project)
                .WithMany(p => p.Lots)
                .HasForeignKey(k => k.ProjectId);
        }
    }
}
