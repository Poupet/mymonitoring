using MyMonitoring.Domain.Entities;
using MyMonitoring.Domain.Entities.Admin;
using MyMonitoring.Domain.Repositories;
using MyMonitoring.Infrastructure.SchemaDefinitions;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using MyMonitoring.Domain.Repositories.Base;
using System;
using Microsoft.AspNetCore.Identity;

namespace MyMonitoring.Infrastructure
{
    public class MonitoringContext : IdentityDbContext<ApplicationUser, IdentityRole<Guid>, Guid>, IUnitOfWork
    {
        public const string DEFAULT_SCHEMA = "monitoring";

        public DbSet<Project> Projects { get; set; }
        public DbSet<Lot> Lots { get; set; }
        public DbSet<Feature> Features { get; set; }
        public DbSet<Issue> Issues { get; set; }

        public MonitoringContext(DbContextOptions<MonitoringContext> options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new ProjectEntitySchemaDefinition());
            modelBuilder.ApplyConfiguration(new LotEntitySchemaDefinition());
            modelBuilder.ApplyConfiguration(new FeatureEntityShemaDefinition());
            modelBuilder.ApplyConfiguration(new IssueEntityShemaDefinition());

            base.OnModelCreating(modelBuilder);
        }

        public async Task<bool> SaveEntitiesAsync(CancellationToken
         cancellationToken = default(CancellationToken))
        {
            await SaveChangesAsync(cancellationToken);
            return true;
        }
    }
}