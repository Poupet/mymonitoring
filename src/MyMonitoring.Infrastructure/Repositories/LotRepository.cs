﻿using Microsoft.EntityFrameworkCore;
using MyMonitoring.Domain.Entities;
using MyMonitoring.Domain.Repositories;
using MyMonitoring.Domain.Repositories.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyMonitoring.Infrastructure.Repositories
{
    public class LotRepository : ILotRepository
    {
        private readonly MonitoringContext _context;

        public IUnitOfWork UnitOfWork => _context;

        public LotRepository(MonitoringContext context)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public Lot Add(Lot lot)
        {
            return _context.Lots
                .Add(lot).Entity;
        }

        public Lot Delete(Lot lot)
        {
            _context.Lots.Remove(lot);
            return lot;
        }

        public async Task<IEnumerable<Lot>> GetAsync()
        {
            return await _context
                .Lots
                .Include(l => l.Project)
                .Include(l => l.Features)
                .AsNoTracking()
                .ToListAsync();
        }

        public async Task<Lot> GetAsync(Guid id)
        {
            var lot = await _context.Lots
                .Include(l => l.Project)
                .Include(l => l.Features).ThenInclude(f => f.Issues)
                .AsNoTracking()
                .Where(x => x.Id == id)
                .FirstOrDefaultAsync();

            return lot;
        }

        public Lot Update(Lot lot)
        {
            _context.Entry(lot).State = EntityState.Modified;
            return _context.Lots.Find(lot);
        }
    }
}
