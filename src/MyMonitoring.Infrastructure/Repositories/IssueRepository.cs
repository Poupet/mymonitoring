﻿using Microsoft.EntityFrameworkCore;
using MyMonitoring.Domain.Entities;
using MyMonitoring.Domain.Repositories;
using MyMonitoring.Domain.Repositories.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyMonitoring.Infrastructure.Repositories
{
    public class IssueRepository : IIssueRepository
    {
        private readonly MonitoringContext _context;

        public IUnitOfWork UnitOfWork => _context;

        public IssueRepository(MonitoringContext context)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public Issue Add(Issue issue)
        {
            return _context.Issues
                .Add(issue).Entity;
        }

        public Issue Delete(Issue issue)
        {
            _context.Issues.Remove(issue);
            return issue;
        }

        public async Task<IEnumerable<Issue>> GetAsync()
        {
            return await _context
                .Issues
                .Include(i => i.Feature).ThenInclude(f => f.Lot).ThenInclude(l => l.Project)
                .AsNoTracking()
                .ToListAsync();
        }

        public async Task<Issue> GetAsync(Guid id)
        {
            var issue = await _context.Issues
                .AsNoTracking()
                .Include(i => i.Feature).ThenInclude(f => f.Lot).ThenInclude(l => l.Project)
                .Where(x => x.Id == id)
                .FirstOrDefaultAsync();

            return issue;
        }

        public Issue Update(Issue issue)
        {
            _context.Entry(issue).State = EntityState.Modified;
            return _context.Issues.Find(issue);
        }
    }
}
