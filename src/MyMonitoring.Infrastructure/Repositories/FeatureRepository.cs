﻿using Microsoft.EntityFrameworkCore;
using MyMonitoring.Domain.Entities;
using MyMonitoring.Domain.Repositories;
using MyMonitoring.Domain.Repositories.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyMonitoring.Infrastructure.Repositories
{
    public class FeatureRepository : IFeatureRepository
    {
        private readonly MonitoringContext _context;

        public IUnitOfWork UnitOfWork => _context;

        public FeatureRepository(MonitoringContext context)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public Feature Add(Feature feature)
        {
            return _context.Features
                .Add(feature).Entity;
        }

        public Feature Delete(Feature feature)
        {
            _context.Features.Remove(feature);
            return feature;
        }

        public async Task<IEnumerable<Feature>> GetAsync()
        {
            return await _context
                .Features
                .Include(f => f.Issues)
                .Include(f => f.Lot).ThenInclude(l => l.Project)
                .AsNoTracking()
                .ToListAsync();
        }

        public async Task<Feature> GetAsync(Guid id)
        {
            var feature = await _context.Features
                .Include(f => f.Issues)
                .Include(f => f.Lot).ThenInclude(l => l.Project)
                .AsNoTracking()
                .Where(x => x.Id == id)
                .FirstOrDefaultAsync();

            return feature;
        }

        public Feature Update(Feature feature)
        {
            _context.Entry(feature).State = EntityState.Modified;
            return _context.Features.Find(feature);
        }
    }
}
