using MyMonitoring.Domain.Entities;
using MyMonitoring.Domain.Repositories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MyMonitoring.Domain.Repositories.Base;

namespace MyMonitoring.Infrastructure.Repositories
{
    public class ProjectRepository : IProjectRepository
    {
        private readonly MonitoringContext _context;

        public IUnitOfWork UnitOfWork => _context;

        public ProjectRepository(MonitoringContext context)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public async Task<IEnumerable<Project>> GetAsync()
        {
            return await _context
                .Projects
                .Include(p => p.Lots)
                .Include(p => p.Owner)
                .AsNoTracking()
                .ToListAsync();
        }

        public async Task<Project> GetAsync(Guid id)
        {
            var project = await _context.Projects
                .AsNoTracking()
                .Include(p => p.Lots)
                .Include(p => p.Owner)
                .Where(x => x.Id == id)
                .FirstOrDefaultAsync();

            return project;
        }

        public Project Add(Project project)
        {
            return _context.Projects
                .Add(project).Entity;
        }

        public Project Update(Project project)
        {
            _context.Entry(project).State = EntityState.Modified;
            return _context.Projects.Find(project);
        }

        public Project Delete(Project project)
        {
            _context.Projects.Remove(project);
            return project;
        }
    }
}