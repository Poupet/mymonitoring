﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MyMonitoring.Domain.Requests.Lot
{
    public class EditLotRequest
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
