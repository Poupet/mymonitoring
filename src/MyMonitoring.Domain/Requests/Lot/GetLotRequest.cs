﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MyMonitoring.Domain.Requests.Lot
{
    public class GetLotRequest
    {
        public Guid Id { get; set; }
    }
}
