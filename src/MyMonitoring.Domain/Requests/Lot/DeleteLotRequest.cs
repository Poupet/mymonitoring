﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MyMonitoring.Domain.Requests.Lot
{
    public class DeleteLotRequest
    {
        public Guid Id { get; set; }
    }
}
