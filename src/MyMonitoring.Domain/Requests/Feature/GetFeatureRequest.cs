﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MyMonitoring.Domain.Requests.Feature
{
    public class GetFeatureRequest
    {
        public Guid Id { get; set; }
    }
}
