﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MyMonitoring.Domain.Requests.Feature
{
    public class AddFeatureRequest
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public Guid LotId { get; set; }
    }
}
