﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MyMonitoring.Domain.Requests.Feature
{
    public class DeleteFeatureRequest
    {
        public Guid Id { get; set; }
    }
}
