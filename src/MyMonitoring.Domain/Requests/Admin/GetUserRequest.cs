﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Text;

namespace MyMonitoring.Domain.Requests.Admin
{
    public class GetUserRequest
    {
        public ClaimsPrincipal User { get; set; }
    }
}
