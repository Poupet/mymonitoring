﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MyMonitoring.Domain.Requests.Admin
{
    public class DeleteUserRequest
    {
        public Guid Id { get; set; }
    }
}
