﻿using System;
using System.Collections.Generic;
using System.Text;
using FluentValidation;
using MyMonitoring.Domain.Repositories;

namespace MyMonitoring.Domain.Requests.Project.Validator
{
    public class AddProjectRequestValidator : AbstractValidator<AddProjectRequest>
    {
        private readonly IProjectRepository _projectRepository;

        public AddProjectRequestValidator(
            IProjectRepository projectRepository
            )
        {
            _projectRepository = projectRepository;

            RuleFor(p => p.Name)
                .NotEmpty();

            RuleFor(p => p.Comment)
                .NotEmpty();

            RuleFor(p => p.StartDate)
                .NotEmpty();

            RuleFor(p => p.EndDate)
                .NotEmpty()
                .GreaterThan(p => p.StartDate);

            RuleFor(p => p.OwnerId)
                .NotEmpty();
        }
    }
}
