﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MyMonitoring.Domain.Requests.Project
{
    public class GetProjectRequest
    {
        public Guid Id { get; set; }
    }
}
