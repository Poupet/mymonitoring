﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MyMonitoring.Domain.Requests.Project
{
    public class DeleteProjectRequest
    {
        public Guid Id { get; set; }
    }
}
