﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MyMonitoring.Domain.Requests.Project
{
    public class AddProjectRequest
    {
        public string Name { get; set; }
        public string Comment { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public Guid OwnerId { get; set; }
    }
}
