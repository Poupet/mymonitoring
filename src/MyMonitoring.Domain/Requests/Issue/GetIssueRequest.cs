﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MyMonitoring.Domain.Requests.Issue
{
    public class GetIssueRequest
    {
        public Guid Id { get; set; }
    }
}
