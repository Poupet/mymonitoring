﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MyMonitoring.Domain.Requests.Issue
{
    public class AddIssueRequest
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public Guid FeatureId { get; set; }
        public bool Finished { get; set; }
        public Guid FinisherId { get; set; }
    }
}
