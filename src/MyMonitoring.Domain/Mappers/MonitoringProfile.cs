using AutoMapper;
using MyMonitoring.Domain.Entities;
using MyMonitoring.Domain.Entities.Admin;
using MyMonitoring.Domain.Requests.Feature;
using MyMonitoring.Domain.Requests.Issue;
using MyMonitoring.Domain.Requests.Lot;
using MyMonitoring.Domain.Requests.Project;
using MyMonitoring.Domain.Responses.ApplicationUser;
using MyMonitoring.Domain.Responses.Feature;
using MyMonitoring.Domain.Responses.Issue;
using MyMonitoring.Domain.Responses.Lot;
using MyMonitoring.Domain.Responses.Project;

namespace MyMonitoring.Domain.Mappers
{
    public class MonitoringProfile : Profile
    {
        public MonitoringProfile()
        {
            CreateMap<ApplicationUserResponse, ApplicationUser>().ReverseMap();

            CreateMap<ProjectResponse, Project>().ReverseMap();
            CreateMap<AddProjectRequest, Project>().ReverseMap();
            CreateMap<EditProjectRequest, Project>().ReverseMap();
            CreateMap<DeleteProjectRequest, Project>().ReverseMap();

            CreateMap<LotResponse, Lot>().ReverseMap();
            CreateMap<AddLotRequest, Lot>().ReverseMap();
            CreateMap<EditLotRequest, Lot>().ReverseMap();
            CreateMap<DeleteLotRequest, Lot>().ReverseMap();

            CreateMap<FeatureResponse, Feature>().ReverseMap();
            CreateMap<AddFeatureRequest, Feature>().ReverseMap();
            CreateMap<EditFeatureRequest, Feature>().ReverseMap();
            CreateMap<DeleteFeatureRequest, Feature>().ReverseMap();

            CreateMap<IssueResponse, Issue>().ReverseMap();
            CreateMap<AddIssueRequest, Issue>().ReverseMap();
            CreateMap<EditIssueRequest, Issue>().ReverseMap();
            CreateMap<DeleteIssueRequest, Issue>().ReverseMap();
        }
    }
}