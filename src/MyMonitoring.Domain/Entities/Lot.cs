﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MyMonitoring.Domain.Entities
{
    public class Lot
    {
        public Guid Id { get; set; }
        
        public string Name { get; set; }
        
        public string Description { get; set; }

        #region FK

        public Guid ProjectId { get; set; }

        public Project Project { get; set; }

        public ICollection<Feature> Features { get; set; }

        #endregion
    }
}
