﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;

namespace MyMonitoring.Domain.Entities.Admin
{
    public class ApplicationUser : IdentityUser<Guid>
    {
        public ICollection<Project> Projects { get; set; }

        public ICollection<Issue> Issues { get; set; }
    }
}
