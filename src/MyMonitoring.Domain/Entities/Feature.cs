﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyMonitoring.Domain.Entities
{
    public class Feature
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        #region FK

        public Guid LotId { get; set; }

        public Lot Lot { get; set; }

        public ICollection<Issue> Issues { get; set; }

        #endregion
    }
}
