﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;
using MyMonitoring.Domain.Entities.Admin;

namespace MyMonitoring.Domain.Entities
{
    public class Project
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Comment { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public Visibility Visibility { get; set; }
        public ApplicationUser Owner { get; set; }
        public virtual Guid OwnerId { get; set; }

        #region FK

        public ICollection<Lot> Lots { get; set; }

        #endregion
    }

    public enum Visibility
    {
        Public,
        Private
    }
}
