﻿using System;
using System.Collections.Generic;
using System.Text;
using MyMonitoring.Domain.Entities.Admin;

namespace MyMonitoring.Domain.Entities
{
    public class Issue
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public bool Finished { get; set; }

        public DateTime EndDate { get; set; }

        #region FK

        public Guid FeatureId { get; set; }

        public Feature Feature { get; set; }

        public Guid FinisherId { get; set; }

        public ApplicationUser Finisher { get; set; }

        #endregion
    }
}
