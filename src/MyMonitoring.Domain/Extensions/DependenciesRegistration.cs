﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;
using AutoMapper;
using MyMonitoring.Domain.Mappers;
using MyMonitoring.Domain.Services;
using FluentValidation.AspNetCore;
using System.Reflection;
using MyMonitoring.Domain.Services.Interfaces;

namespace MyMonitoring.Domain.Extensions
{
    public static class DependenciesRegistration
    {
        public static IServiceCollection AddMappers(this IServiceCollection services)
        {
            // Auto Mapper Configurations
            var mappingConfig = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<MonitoringProfile>();
            });

            IMapper mapper = mappingConfig.CreateMapper();
            services.AddSingleton(mapper);

            return services;
        }

        public static IServiceCollection AddServices(this IServiceCollection services)
        {
            services
                .AddScoped<IProjectService, ProjectService>()
                .AddScoped<ILotService, LotService>()
                .AddScoped<IFeatureService, FeatureService>()
                .AddScoped<IIssueService, IssueService>();

            return services;
        }

        public static IMvcBuilder AddValidation(this IMvcBuilder builder)
        {
            builder
                .AddFluentValidation(configuration =>
                    configuration.RegisterValidatorsFromAssembly
                        (Assembly.GetExecutingAssembly()));

            return builder;
        }
    }
}
