﻿using MyMonitoring.Domain.Responses.ApplicationUser;
using MyMonitoring.Domain.Responses.Feature;
using System;
using System.Collections.Generic;
using System.Text;

namespace MyMonitoring.Domain.Responses.Issue
{
    public class IssueResponse
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public bool Finished { get; set; }

        public DateTime EndDate { get; set; }

        public Guid? FeatureId { get; set; }

        public FeatureResponse? Feature { get; set; }

        public Guid? FinisherId { get; set; }

        public ApplicationUserResponse Finisher { get; set; }
    }
}
