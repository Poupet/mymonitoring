﻿using MyMonitoring.Domain.Responses.Feature;
using MyMonitoring.Domain.Responses.Project;
using System;
using System.Collections.Generic;
using System.Text;

namespace MyMonitoring.Domain.Responses.Lot
{
    public class LotResponse
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public Guid? ProjectId { get; set; }

        public ProjectResponse? Project { get; set; }

        public ICollection<FeatureResponse> Features { get; set; }
    }
}
