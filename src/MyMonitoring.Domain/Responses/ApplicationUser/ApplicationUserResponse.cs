﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MyMonitoring.Domain.Responses.ApplicationUser
{
    public class ApplicationUserResponse
    {
        public Guid Id { get; set; }
        public string UserName { get; set; }
    }
}
