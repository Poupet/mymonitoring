﻿using MyMonitoring.Domain.Responses.ApplicationUser;
using MyMonitoring.Domain.Responses.Lot;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace MyMonitoring.Domain.Responses.Project
{
    public class ProjectResponse
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Comment { get; set; }
        public DateTime StartDate { get; set; } = DateTime.Now;
        public DateTime EndDate { get; set; }
        public ApplicationUserResponse? Owner { get; set; }
        public Guid? OwnerId { get; set; }
        public ICollection<LotResponse> Lots { get; set; }
    }
}
