﻿using MyMonitoring.Domain.Responses.Issue;
using MyMonitoring.Domain.Responses.Lot;
using System;
using System.Collections.Generic;
using System.Text;

namespace MyMonitoring.Domain.Responses.Feature
{
    public class FeatureResponse
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public Guid? LotId { get; set; }

        public LotResponse? Lot { get; set; }

        public ICollection<IssueResponse> Issues { get; set; }

    }
}
