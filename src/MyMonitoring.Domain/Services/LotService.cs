﻿using AutoMapper;
using MyMonitoring.Domain.Entities;
using MyMonitoring.Domain.Repositories;
using MyMonitoring.Domain.Requests.Lot;
using MyMonitoring.Domain.Responses.Lot;
using MyMonitoring.Domain.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyMonitoring.Domain.Services
{
    public class LotService : ILotService
    {
        private readonly ILotRepository _lotRepository;
        private readonly IMapper _mapper;

        public LotService(ILotRepository lotRepository, IMapper mapper)
        {
            _lotRepository = lotRepository;
            _mapper = mapper;
        }
        public async Task<LotResponse> AddLotAsync(AddLotRequest request)
        {
            var lot = _mapper.Map<Lot>(request);
            var result = _lotRepository.Add(lot);

            try
            {
                await _lotRepository.UnitOfWork.SaveChangesAsync();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.Message);
            }
            return _mapper.Map<LotResponse>(result);
        }

        public async Task<LotResponse> DeleteLotAsync(DeleteLotRequest request)
        {
            var existingRecord = await
             _lotRepository.GetAsync(request.Id);

            if (existingRecord == null)
                throw new ArgumentException($"Entity with {request.Id} is not present");

            var entity = _mapper.Map<Lot>(request);
            var result = _lotRepository.Delete(entity);

            await _lotRepository.UnitOfWork.SaveChangesAsync();

            return _mapper.Map<LotResponse>(result);
        }

        public async Task<LotResponse> EditLotAsync(EditLotRequest request)
        {
            var existingRecord = await
             _lotRepository.GetAsync(request.Id);

            if (existingRecord == null)
                throw new ArgumentException($"Entity with {request.Id} is not present");

            var entity = _mapper.Map<Lot>(request);
            var result = _lotRepository.Update(entity);

            await _lotRepository.UnitOfWork.SaveChangesAsync();

            return _mapper.Map<LotResponse>(result);
        }

        public async Task<LotResponse> GetLotAsync(GetLotRequest request)
        {
            if (request?.Id == null) throw new ArgumentNullException();

            var entity = await _lotRepository.GetAsync(request.Id);

            return _mapper.Map<LotResponse>(entity);
        }

        public async Task<IEnumerable<LotResponse>> GetLotsAsync()
        {
            var result = await _lotRepository.GetAsync();

            return result.Select(x => _mapper.Map<LotResponse>(x));
        }
    }
}
