﻿using AutoMapper;
using Microsoft.AspNetCore.Identity;
using MyMonitoring.Domain.Entities.Admin;
using MyMonitoring.Domain.Requests.Admin;
using MyMonitoring.Domain.Responses.ApplicationUser;
using MyMonitoring.Domain.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyMonitoring.Domain.Services
{
    public class AdminService : IAdminService
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IMapper _mapper;

        public AdminService(UserManager<ApplicationUser> userManager, IMapper mapper)
        {
            _userManager = userManager;
            _mapper = mapper;
        }

        public async Task<ApplicationUserResponse> AddUserAsync(AddUserRequest request)
        {
            throw new NotImplementedException();
        }

        public async Task<ApplicationUserResponse> DeleteUserAsync(DeleteUserRequest request)
        {
            throw new NotImplementedException();
        }

        public async Task<ApplicationUserResponse> EditUserAsync(EditUserRequest request)
        {
            throw new NotImplementedException();
        }

        public async Task<ApplicationUserResponse> GetUserAsync(GetUserRequest request)
        {
            var entity = _userManager.GetUserAsync(request.User);

            return _mapper.Map<ApplicationUserResponse>(entity);
        }

        public async Task<IEnumerable<ApplicationUserResponse>> GetUsersAsync()
        {
            var result = _userManager.Users;

            return result.Select(x => _mapper.Map<ApplicationUserResponse>(x));
        }
    }
}
