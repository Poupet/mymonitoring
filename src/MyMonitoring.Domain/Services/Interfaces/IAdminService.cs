﻿using MyMonitoring.Domain.Requests.Admin;
using MyMonitoring.Domain.Responses.ApplicationUser;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace MyMonitoring.Domain.Services.Interfaces
{
    public interface IAdminService
    {
        Task<IEnumerable<ApplicationUserResponse>> GetUsersAsync();
        Task<ApplicationUserResponse> GetUserAsync(GetUserRequest request);
        Task<ApplicationUserResponse> AddUserAsync(AddUserRequest request);
        Task<ApplicationUserResponse> EditUserAsync(EditUserRequest request);
        Task<ApplicationUserResponse> DeleteUserAsync(DeleteUserRequest request);
    }
}
