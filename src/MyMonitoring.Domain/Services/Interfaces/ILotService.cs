using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using MyMonitoring.Domain.Requests.Lot;
using MyMonitoring.Domain.Responses;
using MyMonitoring.Domain.Responses.Lot;

namespace MyMonitoring.Domain.Services.Interfaces
{
    public interface ILotService
    {
        Task<IEnumerable<LotResponse>> GetLotsAsync();
        Task<LotResponse> GetLotAsync(GetLotRequest request);
        Task<LotResponse> AddLotAsync(AddLotRequest request);
        Task<LotResponse> EditLotAsync(EditLotRequest request);
        Task<LotResponse> DeleteLotAsync(DeleteLotRequest request);
    }
}