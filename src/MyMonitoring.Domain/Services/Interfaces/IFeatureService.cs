﻿using MyMonitoring.Domain.Requests.Feature;
using MyMonitoring.Domain.Responses.Feature;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace MyMonitoring.Domain.Services.Interfaces
{
    public interface IFeatureService
    {
        Task<IEnumerable<FeatureResponse>> GetFeaturesAsync();
        Task<FeatureResponse> GetFeatureAsync(GetFeatureRequest request);
        Task<FeatureResponse> AddFeatureAsync(AddFeatureRequest request);
        Task<FeatureResponse> EditFeatureAsync(EditFeatureRequest request);
        Task<FeatureResponse> DeleteFeatureAsync(DeleteFeatureRequest request);
    }
}
