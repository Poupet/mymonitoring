using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using MyMonitoring.Domain.Requests.Project;
using MyMonitoring.Domain.Responses;
using MyMonitoring.Domain.Responses.Project;

namespace MyMonitoring.Domain.Services.Interfaces
{
    public interface IProjectService
    {
        Task<IEnumerable<ProjectResponse>> GetProjectsAsync();
        Task<ProjectResponse> GetProjectAsync(GetProjectRequest request);
        Task<ProjectResponse> AddProjectAsync(AddProjectRequest request);
        Task<ProjectResponse> EditProjectAsync(EditProjectRequest request);
        Task<ProjectResponse> DeleteProjectAsync(DeleteProjectRequest request);
    }
}