﻿using MyMonitoring.Domain.Requests.Issue;
using MyMonitoring.Domain.Responses.Issue;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace MyMonitoring.Domain.Services.Interfaces
{
    public interface IIssueService
    {
        Task<IEnumerable<IssueResponse>> GetIssuesAsync();
        Task<IssueResponse> GetIssueAsync(GetIssueRequest request);
        Task<IssueResponse> AddIssueAsync(AddIssueRequest request);
        Task<IssueResponse> EditIssueAsync(EditIssueRequest request);
        Task<IssueResponse> DeleteIssueAsync(DeleteIssueRequest request);
    }
}
