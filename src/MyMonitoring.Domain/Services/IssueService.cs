﻿using AutoMapper;
using MyMonitoring.Domain.Entities;
using MyMonitoring.Domain.Repositories;
using MyMonitoring.Domain.Requests.Issue;
using MyMonitoring.Domain.Responses.Issue;
using MyMonitoring.Domain.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyMonitoring.Domain.Services
{
    public class IssueService : IIssueService
    {
        private readonly IIssueRepository _issueRepository;
        private readonly IMapper _mapper;

        public IssueService(IIssueRepository issueRepository, IMapper mapper)
        {
            _issueRepository = issueRepository;
            _mapper = mapper;
        }
        public async Task<IssueResponse> AddIssueAsync(AddIssueRequest request)
        {
            var issue = _mapper.Map<Issue>(request);
            var result = _issueRepository.Add(issue);

            try
            {
                await _issueRepository.UnitOfWork.SaveChangesAsync();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.Message);
            }
            return _mapper.Map<IssueResponse>(result);
        }

        public async Task<IssueResponse> DeleteIssueAsync(DeleteIssueRequest request)
        {
            var existingRecord = await
             _issueRepository.GetAsync(request.Id);

            if (existingRecord == null)
                throw new ArgumentException($"Entity with {request.Id} is not present");

            var entity = _mapper.Map<Issue>(request);
            var result = _issueRepository.Delete(entity);

            await _issueRepository.UnitOfWork.SaveChangesAsync();

            return _mapper.Map<IssueResponse>(result);
        }

        public async Task<IssueResponse> EditIssueAsync(EditIssueRequest request)
        {
            var existingRecord = await
             _issueRepository.GetAsync(request.Id);

            if (existingRecord == null)
                throw new ArgumentException($"Entity with {request.Id} is not present");

            var entity = _mapper.Map<Issue>(request);
            var result = _issueRepository.Update(entity);

            await _issueRepository.UnitOfWork.SaveChangesAsync();

            return _mapper.Map<IssueResponse>(result);
        }

        public async Task<IssueResponse> GetIssueAsync(GetIssueRequest request)
        {
            if (request?.Id == null) throw new ArgumentNullException();

            var entity = await _issueRepository.GetAsync(request.Id);

            return _mapper.Map<IssueResponse>(entity);
        }

        public async Task<IEnumerable<IssueResponse>> GetIssuesAsync()
        {
            var result = await _issueRepository.GetAsync();

            return result.Select(x => _mapper.Map<IssueResponse>(x));
        }
    }
}
