﻿using AutoMapper;
using MyMonitoring.Domain.Entities;
using MyMonitoring.Domain.Repositories;
using MyMonitoring.Domain.Requests.Feature;
using MyMonitoring.Domain.Responses.Feature;
using MyMonitoring.Domain.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyMonitoring.Domain.Services
{
    public class FeatureService : IFeatureService
    {
        private readonly IFeatureRepository _featureRepository;
        private readonly IMapper _mapper;

        public FeatureService(IFeatureRepository featureRepository, IMapper mapper)
        {
            _featureRepository = featureRepository;
            _mapper = mapper;
        }
        public async Task<FeatureResponse> AddFeatureAsync(AddFeatureRequest request)
        {
            var feature = _mapper.Map<Feature>(request);
            var result = _featureRepository.Add(feature);

            try
            {
                await _featureRepository.UnitOfWork.SaveChangesAsync();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.Message);
            }
            return _mapper.Map<FeatureResponse>(result);
        }

        public async Task<FeatureResponse> DeleteFeatureAsync(DeleteFeatureRequest request)
        {
            var existingRecord = await
             _featureRepository.GetAsync(request.Id);

            if (existingRecord == null)
                throw new ArgumentException($"Entity with {request.Id} is not present");

            var entity = _mapper.Map<Feature>(request);
            var result = _featureRepository.Delete(entity);

            await _featureRepository.UnitOfWork.SaveChangesAsync();

            return _mapper.Map<FeatureResponse>(result);
        }

        public async Task<FeatureResponse> EditFeatureAsync(EditFeatureRequest request)
        {
            var existingRecord = await
             _featureRepository.GetAsync(request.Id);

            if (existingRecord == null)
                throw new ArgumentException($"Entity with {request.Id} is not present");

            var entity = _mapper.Map<Feature>(request);
            var result = _featureRepository.Update(entity);

            await _featureRepository.UnitOfWork.SaveChangesAsync();

            return _mapper.Map<FeatureResponse>(result);
        }

        public async Task<FeatureResponse> GetFeatureAsync(GetFeatureRequest request)
        {
            if (request?.Id == null) throw new ArgumentNullException();

            var entity = await _featureRepository.GetAsync(request.Id);

            return _mapper.Map<FeatureResponse>(entity);
        }

        public async Task<IEnumerable<FeatureResponse>> GetFeaturesAsync()
        {
            var result = await _featureRepository.GetAsync();

            return result.Select(x => _mapper.Map<FeatureResponse>(x));
        }
    }
}
