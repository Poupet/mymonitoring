using AutoMapper;
using MyMonitoring.Domain.Entities;
using MyMonitoring.Domain.Repositories;
using MyMonitoring.Domain.Requests.Project;
using MyMonitoring.Domain.Responses.Project;
using MyMonitoring.Domain.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyMonitoring.Domain.Services
{
    public class ProjectService : IProjectService
    {
        private readonly IProjectRepository _projectRepository;
        private readonly IMapper _mapper;

        public ProjectService(IProjectRepository projectRepository, IMapper mapper)
        {
            _projectRepository = projectRepository;
            _mapper = mapper;
        }

        public async Task<IEnumerable<ProjectResponse>> GetProjectsAsync()
        {
            var result = await _projectRepository.GetAsync();

            return result.Select(x => _mapper.Map<ProjectResponse>(x));
        }

        public async Task<ProjectResponse> GetProjectAsync(GetProjectRequest request)
        {
            if (request?.Id == null) throw new ArgumentNullException();

            var entity = await _projectRepository.GetAsync(request.Id);

            return _mapper.Map<ProjectResponse>(entity);
        }

        public async Task<ProjectResponse> AddProjectAsync(AddProjectRequest request)
        {
            var project = _mapper.Map<Project>(request);
            var result = _projectRepository.Add(project);

            try
            {
                await _projectRepository.UnitOfWork.SaveChangesAsync();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.Message);
            }
            return _mapper.Map<ProjectResponse>(result);
        }

        public async Task<ProjectResponse> EditProjectAsync(EditProjectRequest request)
        {
            var existingRecord = await
             _projectRepository.GetAsync(request.Id);

            if (existingRecord == null)
                throw new ArgumentException($"Entity with {request.Id} is not present");

            var entity = _mapper.Map<Project>(request);
            var result = _projectRepository.Update(entity);

            await _projectRepository.UnitOfWork.SaveChangesAsync();

            return _mapper.Map<ProjectResponse>(result);
        }

        public async Task<ProjectResponse> DeleteProjectAsync(DeleteProjectRequest request)
        {   
            var existingRecord = await
             _projectRepository.GetAsync(request.Id);

             if (existingRecord == null)
                throw new ArgumentException($"Entity with {request.Id} is not present");

            var entity = _mapper.Map<Project>(request);
            var result = _projectRepository.Delete(entity);

            await _projectRepository.UnitOfWork.SaveChangesAsync();

            return _mapper.Map<ProjectResponse>(result);

        }
    }
}