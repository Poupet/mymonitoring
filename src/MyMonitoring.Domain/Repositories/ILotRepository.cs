﻿using MyMonitoring.Domain.Entities;
using MyMonitoring.Domain.Repositories.Base;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace MyMonitoring.Domain.Repositories
{
    public interface ILotRepository : IRepository
    {
        Task<IEnumerable<Lot>> GetAsync();

        Task<Lot> GetAsync(Guid id);

        Lot Add(Lot lot);

        Lot Update(Lot lot);

        Lot Delete(Lot lot);
    }
}
