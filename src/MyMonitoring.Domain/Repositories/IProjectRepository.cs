using MyMonitoring.Domain.Entities;
using MyMonitoring.Domain.Repositories.Base;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MyMonitoring.Domain.Repositories
{
    public interface IProjectRepository : IRepository
    {
        Task<IEnumerable<Project>> GetAsync();

        Task<Project> GetAsync(Guid id);

        Project Add(Project project);

        Project Update(Project project);

        Project Delete(Project project);
    }
}