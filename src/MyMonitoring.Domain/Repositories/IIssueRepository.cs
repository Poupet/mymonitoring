﻿using MyMonitoring.Domain.Entities;
using MyMonitoring.Domain.Repositories.Base;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace MyMonitoring.Domain.Repositories
{
    public interface IIssueRepository : IRepository
    {
        Task<IEnumerable<Issue>> GetAsync();

        Task<Issue> GetAsync(Guid id);

        Issue Add(Issue issue);

        Issue Update(Issue issue);

        Issue Delete(Issue issue);
    }
}
