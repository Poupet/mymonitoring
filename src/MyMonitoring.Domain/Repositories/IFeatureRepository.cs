﻿using MyMonitoring.Domain.Entities;
using MyMonitoring.Domain.Repositories.Base;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace MyMonitoring.Domain.Repositories
{
    public interface IFeatureRepository : IRepository
    {
        Task<IEnumerable<Feature>> GetAsync();

        Task<Feature> GetAsync(Guid id);

        Feature Add(Feature feature);

        Feature Update(Feature feature);

        Feature Delete(Feature feature);
    }
}
