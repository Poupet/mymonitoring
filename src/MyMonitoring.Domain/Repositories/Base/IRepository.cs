﻿namespace MyMonitoring.Domain.Repositories.Base
{
    public interface IRepository
    {
        IUnitOfWork UnitOfWork { get; }
    }
}