﻿using MyMonitoring.Fixtures;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace MyMonitoring.Web.Tests.Controllers
{
    public class ProjectControllerTests : IClassFixture<InMemoryApplicationFactory<Startup>>
    {
        private readonly InMemoryApplicationFactory<Startup> _factory;

        public ProjectControllerTests(InMemoryApplicationFactory<Startup> factory)
        {
            _factory = factory;
        }
    }
}
