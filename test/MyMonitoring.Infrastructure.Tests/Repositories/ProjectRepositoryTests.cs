﻿using AutoMapper;
using MyMonitoring.Fixtures;
using MyMonitoring.Infrastructure.Repositories;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace MyMonitoring.Infrastructure.Tests.Repositories
{
    public class ProjectRepositoryTests : IClassFixture<MonitoringContextFactory>, IClassFixture<MapperFactory>
    {
        private readonly ProjectRepository _sut;
        private readonly TestMonitoringContext _context;
        private readonly IMapper _mapper;

        public ProjectRepositoryTests(MonitoringContextFactory monitoringContextFactory, MapperFactory mapperFactory)
        {
            _context = monitoringContextFactory.ContextInstance;
            _sut = new ProjectRepository(_context);

            _mapper = mapperFactory.Mapper;
        }
    }
}
