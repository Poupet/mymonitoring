﻿using AutoMapper;
using MyMonitoring.Fixtures;
using MyMonitoring.Infrastructure.Repositories;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace MyMonitoring.Domain.Tests.Services
{
    public class ProjectServiceTests : IClassFixture<MonitoringContextFactory>, IClassFixture<MapperFactory>
    {
        private readonly ProjectRepository _projectRepository;

        private readonly IMapper _mapper;

        public ProjectServiceTests(MonitoringContextFactory monitoringContextFactory, MapperFactory mapperFactory)
        {
            _projectRepository = new ProjectRepository(monitoringContextFactory.ContextInstance);
            _mapper = mapperFactory.Mapper;
        }
    }
}
