﻿using AutoMapper;
using MyMonitoring.Domain.Mappers;

namespace MyMonitoring.Fixtures
{
    public class MapperFactory
    {
        public readonly Mapper Mapper;

        public MapperFactory()
        {
            var configuration = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<MonitoringProfile>();
            });

            Mapper = new Mapper(configuration);
        }
    }
}