using System;
using AutoMapper;
using MyMonitoring.Domain.Mappers;
using MyMonitoring.Infrastructure;
using Microsoft.EntityFrameworkCore;

namespace MyMonitoring.Fixtures
{
    public class MonitoringContextFactory
    {
        public readonly TestMonitoringContext ContextInstance;

        public MonitoringContextFactory()
        {
            // Using Guid.NewGuid().ToString() property as a database name in order to provide a new, clean in-memory instance for each test class
            var contextOptions = new
                DbContextOptionsBuilder<MonitoringContext>()
                .UseInMemoryDatabase(Guid.NewGuid().ToString())
                .EnableSensitiveDataLogging()
                .Options;

            EnsureCreation(contextOptions);
            ContextInstance = new TestMonitoringContext(contextOptions);
        }

        private void EnsureCreation(DbContextOptions<MonitoringContext>
         contextOptions)
        {
            using var context = new TestMonitoringContext(contextOptions);
            context.Database.EnsureCreated();
        }
    }
}