using MyMonitoring.Domain.Entities;
using MyMonitoring.Fixtures.Extensions;
using MyMonitoring.Infrastructure;
using Microsoft.EntityFrameworkCore;

namespace MyMonitoring.Fixtures
{
    public class TestMonitoringContext : MonitoringContext
    {
        public TestMonitoringContext(DbContextOptions<MonitoringContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            //modelBuilder.Seed<Artist>("./Data/artist.json");
            //modelBuilder.Seed<Genre>("./Data/genre.json");
            //modelBuilder.Seed<Item>("./Data/item.json");
        }
    }
}